<?php

namespace Ionux\PHPDocMd;

use ReflectionClass;

class PHPDocMD
{
    /**
     * 
     * 
     * 
     */
    public static function reflectionClassToMD(ReflectionClass $reflectionClass) : string
    {
        $classFullName = $reflectionClass->getName();
        $fileName = $reflectionClass->getFileName();
        $fileStartLine = $reflectionClass->getStartLine();
        $fileEndLine = $reflectionClass->getEndLine();

        $header = static::link($classFullName, "$fileName#L$fileStartLine-$fileEndLine");
        
        return 'Hello world!';
    }

    /**
     * 
     * 
     * 
     */
    public static function link(string $text, string $url) : string
    {
        return "[$text]($url)";
    }
}